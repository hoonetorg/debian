ROOT_DIR := $(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))
NAME := $(shell basename $(ROOT_DIR))

ifdef REG
else
REG := quay.io
endif
ifdef REG_NS
else
REG_NS := hoocloud
endif

ifdef APIKEY__QUAY_IO
#else
#APIKEY__QUAY_IO := quay.io/hoocloud
endif

MAKE := make --no-print-directory

VERSION_FILE := VERSION
VERSION := $(shell cat $(VERSION_FILE) )

container:
	for DIST in buster bullseye bookworm; do \
		mkdir -p $$DIST $$DIST-slim ; \
		wget -O $$DIST/rootfs.tar.xz https://github.com/debuerreotype/docker-debian-artifacts/raw/dist-amd64/$$DIST/rootfs.tar.xz ; \
		wget -O $$DIST/Dockerfile https://github.com/debuerreotype/docker-debian-artifacts/raw/dist-amd64/$$DIST/Dockerfile ; \
		cd $(ROOT_DIR)/$$DIST ; \
		buildah bud --layers -t $(REG)/$(REG_NS)/$(NAME):$$DIST ; \
		buildah tag $(REG)/$(REG_NS)/$(NAME):$$DIST $(REG)/$(REG_NS)/$(NAME):$$DIST-$(VERSION) ; \
		cd $(ROOT_DIR) ; \
		wget -O $$DIST-slim/rootfs.tar.xz  https://github.com/debuerreotype/docker-debian-artifacts/raw/dist-amd64/$$DIST/slim/rootfs.tar.xz ; \
		wget -O $$DIST-slim/Dockerfile https://github.com/debuerreotype/docker-debian-artifacts/raw/dist-amd64/$$DIST/slim/Dockerfile ; \
		cd $(ROOT_DIR)/$$DIST-slim ; \
		buildah bud --layers -t $(REG)/$(REG_NS)/$(NAME):$$DIST-slim ; \
		buildah tag $(REG)/$(REG_NS)/$(NAME):$$DIST-slim $(REG)/$(REG_NS)/$(NAME):$$DIST-slim-$(VERSION) ; \
		cd $(ROOT_DIR) ; \
	done

.PHONY: gitpush
gitpush:
	git tag $(VERSION)
	git push
	git push --tags

.PHONY: gitpushver
gitpushver: 
	git add VERSION
	git commit -m "version $(VERSION)" VERSION
	git tag $(VERSION)
	git push
	git push --tags

.PHONY: regpush
regpush:
	curl -X POST https://$(REG)/api/v1/repository -d '{"namespace":"'$(REG_NS)'","repository":"'$(NAME)'","description":"Container image '$(NAME)'","visibility":"public"}' -H 'Authorization: Bearer '$(APIKEY__QUAY_IO)'' -H "Content-Type: application/json"
	for DIST in buster bullseye bookworm; do \
		buildah push $(REG)/$(REG_NS)/$(NAME):$$DIST ; \
		buildah push $(REG)/$(REG_NS)/$(NAME):$$DIST-slim ; \
		buildah push $(REG)/$(REG_NS)/$(NAME):$$DIST-$(VERSION) ; \
		buildah push $(REG)/$(REG_NS)/$(NAME):$$DIST-slim-$(VERSION) ; \
	done

.PHONY: regpushrm
regpushrm:
	## -e HOME due to bug in crun when HOME not set, see https://github.com/containers/podman/issues/9378, lmgtfy: "exec: getent: executable file not found in $PATH" crun
	## actually it seems to be a bug, that HOME is set always with runc and the container chko/docker-pushrm:1 is not built correctly
	#podman run --rm -t -v $(ROOT_DIR):/myvol -e HOME -e DOCKER_APIKEY='$(APIKEY__QUAY_IO)' docker.io/chko/docker-pushrm:1 --file /myvol/README.md --provider quay --debug $(REG)/$(REG_NS)/$(NAME)
	podman run --rm -t -v $(ROOT_DIR):/myvol -e HOME -e APIKEY__QUAY_IO='$(APIKEY__QUAY_IO)' docker.io/chko/docker-pushrm:1 --file /myvol/README.md --provider quay --debug $(REG)/$(REG_NS)/$(NAME)
